using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Editor
{
    [InitializeOnLoad]
    public class SceneSwitcherTool
    {
        private const string MenuNameToggle = "Tools/Scene Switcher/Enable Scene Switching";
        private const string MenuNameToggleRestore = "Tools/Scene Switcher/Enable Scene Restoration";
        private const string MenuNameSetStartScenes = "Tools/Scene Switcher/Set Current Scenes as Start Scenes";
        private const string PrefKeyEnable = "EnableSceneSwitching";
        private const string PrefKeyEnableRestore = "EnableSceneRestore";
        private const string SceneSaveCachePath = "scenePathsBeforePlay.txt";
        private const string PrefKeyStartScenes = "StartScenesPaths";
        private const char Separator = ';';
        private static List<string> _scenePathsBeforePlay = new();

        static SceneSwitcherTool()
        {
            EditorApplication.playModeStateChanged += OnPlayModeChanged;
        }


        [InitializeOnLoadMethod]
        private static void Initialize()
        {
            Menu.SetChecked(MenuNameToggle, EditorPrefs.GetBool(PrefKeyEnable, true));
        }


        [MenuItem(MenuNameToggle)]
        private static void ToggleAction()
        {
            var enabled = !Menu.GetChecked(MenuNameToggle);
            Menu.SetChecked(MenuNameToggle, enabled);
            EditorPrefs.SetBool(PrefKeyEnable, enabled);
        }


        [InitializeOnLoadMethod]
        private static void InitializeRestoreToggle()
        {
            Menu.SetChecked(MenuNameToggle, EditorPrefs.GetBool(PrefKeyEnableRestore, true));
        }


        [MenuItem(MenuNameToggleRestore)]
        private static void ToggleRestore()
        {
            var enabled = !Menu.GetChecked(MenuNameToggleRestore);
            Menu.SetChecked(MenuNameToggleRestore, enabled);
            EditorPrefs.SetBool(PrefKeyEnableRestore, enabled);
        }


        [MenuItem(MenuNameSetStartScenes)]
        private static void SetCurrentScenesAsStart()
        {
            var sceneCount = SceneManager.sceneCount;
            var scenePaths = new string[sceneCount];

            for (var i = 0; i < sceneCount; i++)
            {
                var scene = SceneManager.GetSceneAt(i);
                scenePaths[i] = scene.path;
            }

            var concatenatedPaths = string.Join(Separator.ToString(), scenePaths);
            EditorPrefs.SetString(PrefKeyStartScenes, concatenatedPaths);
        }


        private static void OnPlayModeChanged(PlayModeStateChange state)
        {
            if (!EditorPrefs.GetBool(PrefKeyEnable, true)) return;

            switch (state)
            {
                case PlayModeStateChange.ExitingEditMode:
                    SaveCurrentScenes();
                    LoadStartScene();
                    break;
                case PlayModeStateChange.EnteredEditMode:
                    if (!EditorPrefs.GetBool(PrefKeyEnableRestore, true)) return;
                    RestoreScenes();
                    break;
            }
        }

        private static void SaveCurrentScenes()
        {
            _scenePathsBeforePlay.Clear();
            for (var i = 0; i < EditorSceneManager.loadedSceneCount; i++)
            {
                var scene = SceneManager.GetSceneAt(i);
                _scenePathsBeforePlay.Add(scene.path);
            }

            SaveScenePathsToFile();
        }

        private static void SaveScenePathsToFile()
        {
            var tempFilePath = Path.Combine(Application.temporaryCachePath, SceneSaveCachePath);
            File.WriteAllLines(tempFilePath, _scenePathsBeforePlay);
        }


        private static void LoadStartScene()
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            var concatenatedPaths = EditorPrefs.GetString(PrefKeyStartScenes, "");
            if (string.IsNullOrEmpty(concatenatedPaths))
            {
                Debug.LogWarning("No start scenes have been set.");
                return;
            }

            var scenePaths = concatenatedPaths.Split(Separator);
            if (scenePaths.Length > 0)
            {
                if (SceneManager.GetActiveScene().path == scenePaths[0]) return;
                EditorSceneManager.OpenScene(scenePaths[0], OpenSceneMode.Single);
                for (var i = 1; i < scenePaths.Length; i++)
                    if (!string.IsNullOrEmpty(scenePaths[i]))
                        EditorSceneManager.OpenScene(scenePaths[i], OpenSceneMode.Additive);
            }
        }


        private static void RestoreScenes()
        {
            LoadScenePathsFromFile();
            EditorApplication.delayCall += () =>
            {
                // Ensure there are scenes to restore
                if (_scenePathsBeforePlay.Count > 0)
                {
                    // Load the first scene in single mode to clear current scenes
                    var firstScenePath = _scenePathsBeforePlay[0];
                    if (!string.IsNullOrEmpty(firstScenePath) && File.Exists(firstScenePath))
                    {
                        EditorSceneManager.OpenScene(firstScenePath, OpenSceneMode.Single);
                        // Load any additional scenes additively
                        for (var i = 1; i < _scenePathsBeforePlay.Count; i++)
                        {
                            var scenePath = _scenePathsBeforePlay[i];
                            if (!string.IsNullOrEmpty(scenePath) && File.Exists(scenePath))
                                EditorSceneManager.OpenScene(scenePath, OpenSceneMode.Additive);
                        }
                    }
                }
            };
        }


        private static void LoadScenePathsFromFile()
        {
            var tempFilePath = Path.Combine(Application.temporaryCachePath, SceneSaveCachePath);
            if (File.Exists(tempFilePath))
            {
                _scenePathsBeforePlay = new List<string>(File.ReadAllLines(tempFilePath));
                File.Delete(tempFilePath);
            }
        }
    }
}