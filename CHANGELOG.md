# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.2] - 2024-03-27

### Fixed
- Improved project structure to change import and make it more organized

## [1.0.1] - 2024-03-27

### Fixed
- Fixed the assembly definition, as it would throw exceptions on build

## [1.0] - 2024-03-26

### Added

- Initial release of the package.
- Added support for Unity 2021.3 and newer versions.