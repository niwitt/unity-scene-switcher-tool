# Unity Editor Scene Switcher

The Unity Editor Scene Switcher is a tool designed to enhance the development workflow in Unity by providing flexible scene switching and restoration capabilities directly from the Unity Editor. This tool allows developers to quickly set start scenes, toggle scene switching on play mode entry, and restore previously opened scenes upon exiting play mode. It's especially useful for projects where working with multiple scenes simultaneously is common, helping to streamline the testing and development process.

## Features

- **Toggle Scene Switching**: Enable or disable automatic scene switching to a predefined start scene(s) when entering play mode.
- **Toggle Scene Restoration**: Enable or disable the restoration of scenes that were open before entering play mode upon exiting play mode.
- **Set Current Scenes as Start Scenes**: Easily configure one or multiple scenes as the start scenes for the project, which are automatically loaded when entering play mode.
- **Persistence**: Settings and configurations are saved across editor sessions, making your workflow seamless and consistent.

## Installation

### Via Unity Package Manager

* In Unity, Click on Window
    * Package Manager
    * Click "+"
    * Click "Add package from git URL"
    ```
    https://gitlab.com/niwitt/unity-scene-switcher-tool.git?path=/StartSceneSwitcher
    ```  
    * Click "Add"

### Via Copy/Paste
1. Clone this repository or download the ZIP file.
2. If you haven't already, create an `Editor` folder within the `Assets` directory of your Unity project.
3. Copy the `SceneSwitcherTool.cs` script into the `Assets/Editor` directory.

## How to Use

Once installed, the Scene Switcher functionalities can be accessed from the Unity Editor menu under `Tools/Scene Switcher`.

### Setting Start Scenes

1. Open the scenes in Unity Editor that you want to set as start scenes.
2. Navigate to `Tools/Scene Switcher/Set Current Scenes as Start Scenes` and click it.
3. The currently open scenes are now set as the start scenes. Whenever you enter play mode, these scenes will automatically load.

### Toggling Scene Switching

- To toggle the automatic scene switching feature, go to `Tools/Scene Switcher/Enable Scene Switching`. A checkmark next to the menu item indicates the feature is enabled.

### Toggling Scene Restoration

- To toggle the scene restoration feature, navigate to `Tools/Scene Switcher/Enable Scene Restoration`. A checkmark next to the menu item indicates the feature is enabled.

## Compatibility

This script has been tested with Unity 2021.3.4f1 but should be compatible with most versions of Unity.